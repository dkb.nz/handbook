| Date       | Host                 | Presenter 1            | Presenter 2            | Presenter 3      |
|------------|----------------------|------------------------|------------------------|------------------|
| 2024-01-24 | Marcel van Remmerden | Sascha Eggenberger     | Mike Nichols         |                  |
| 2024-02-07 | APAC                 | Michael Le             | Alex Fracazo           |                  |
| 2024-02-21 | Taurie Davis         | Annabel Gray           | Sunjung Park           | Emily Bauman     |
| 2024-03-06 | Emily Sybrant        | Michael Fangman        | Dan Mizzi-Harris       |                  |
| 2024-03-20 | Jacki Bauer          | Chad Lavimoniere       | Matthew Nearents       |                  |
| 2024-04-03 | Justin Mandell       | Julia Miocene          | Amelia Bauerly      | Nick Brandt      |
| 2024-04-17 | Chris Micek          | Austin Regnery         | Taylor Vanderhelm       |                  |
| 2024-05-01 | Andy Volpe           | Gina Doyle             | Camellia X Yang        |                  |
| 2024-05-15 | Rayana Verissimo     | Nick Leonard           | Pedro Moreira da Silva | Becka Lippert    |
| 2024-05-29 | Paul Wright          | Pedro Moreira da Silva | Jeremy Elder           |                  |
| 2024-06-05 | Marcel van Remmerden | Veethika Mishra        | Sascha Eggenberger     |                  |
| 2024-06-26 | APAC                 | Libor Vanc             | Katie Macoy            | Bonnie Tsang     |
| 2024-07-03 | Taurie Davis         | Tim Noah               | Austin Regnery         | Graham Bachelder |
| 2024-07-24 | Emily Sybrant        | Mike Nichols           | Gina Doyle             |                  |
| 2024-08-07 | Jacki Bauer          | Amelia Bauerly         | Michael Fangman        | Ian Gloude       |
| 2024-08-21 | Justin Mandell       | Chad Lavimoniere       | Dan Mizzi-Harris       | Ilonah Pelaez    |
| 2024-09-04 | Chris Micek          | Matthew Nearents       | Becka Lippert          |                  |
| 2024-09-18 | Andy Volpe           | Nick Leonard           | Camellia X Yang        |                  |
| 2024-10-02 | Rayana Verissimo     | Sunjung Park           | Annabel Gray           | Nick Brandt      |
| 2024-10-16 | Paul Wright          | Emily Bauman           | New hire (Switchboard) |                  |
| 2024-10-30 | Taurie Davis         | Jeremy Elder           | Taylor Vanderhelm      |                  |
| 2024-11-06 | APAC                 | Alex Fracazo           | Bonnie Tsang           | Katie Macoy      |
| 2024-11-27 | Marcel van Remmerden | Tim Noah               | Veethika Mishra        |                  |
| 2024-12-11 | Emily Sybrant        | Julia Miocene          | Ian Gloude             | Ilonah Pelaez    |
| 2024-12-18 | Jacki Bauer          | Graham Bachelder       | Libor Vanc             | New hire (TBD)   |

Tip for Product Design Managers: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online [markdown generator](https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table).